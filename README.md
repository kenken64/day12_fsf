http://www.w3schools.com/sql/sql_syntax.asp

https://www.npmjs.com/package/mysql

CURDATE() - SQL get current date
DATE() - Convert to String to Date
YEAR() - Convert date to Year

SQL Operators
--------------
Operator	Description
=	Equal
<>	Not equal. Note: In some versions of SQL this operator may be written as !=
>	Greater than
<	Less than
>=	Greater than or equal
<=	Less than or equal
BETWEEN	Between an inclusive range
LIKE	Search for a pattern
IN	To specify multiple possible values for a column
 